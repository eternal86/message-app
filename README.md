Messange-App with REST API server written in Python and web client written in Javascript(Bootstrap.js + Knockout.js)
=============

Important notice:
-----
Due not being familiar at all with Bootstrap.js and Knockout.js, i spent around 8 hours on front-end, i didn't stick to any rules for HTML and JS.
Please do NOT rate front-end. Also, because of poor client-side, i was not able to do perfect python code.

Time spent
-----
Front-end: 8 hrs
Back-end: 6 hrs

Setup
-----

- Install Python 2.7.x
- Run `setup.sh` (Linux, OS X, Cygwin)
- Go to `app` folder
- Run virtualenv (`. message_app/flask/bin/activate`)
- Configure local postgres access (`postgresql://messanger_usr:messanger_pwd@localhost/messanger`) or edit `message_app/__init.py__`: `app.config['SQLALCHEMY_DATABASE_URI']`
- Run `python _install_migrate.py db init`
- Run `python _install_migrate.py db migrate`
- Run `python _install_migrate.py db upgrade`
- Run `python _install_fill_db.py`
- Run `runserver.py` to start the server
- Open `http://localhost:5000/index.html` on your web browser to run the client(user@password pairs: admin@admin, user_1@user_1, user_2@user_2, user_3@user_3, user_4@user_4, user_5@user_5)

Notice:
-----
- Tested only with Firefox