flask==0.10
flask-httpauth==2.5.0
flask-sqlalchemy==2.0
datetime==4.0.1
flask-api==0.6.3
psycopg2==2.6
flask-script==2.0.5
flask-migrate==1.4.0