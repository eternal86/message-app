from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_url_path="")
app.config['PROPAGATE_EXCEPTIONS'] = True
app.secret_key = 'YNCQXyPQ18w33LI7MrftgNPI2jwgPL_V0yMnxdf'

app.config["project_title"] = 'Messange-app'
# example: dir/
app.config["dir_link"] = ''

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://messanger_usr:messanger_pwd@localhost/messanger'

from models import db
db.init_app(app)
import message_app.routes
