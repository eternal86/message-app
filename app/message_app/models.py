from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
# from time import strftime
from sqlalchemy import Column, Integer, String, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship, backref
from datetime import datetime

db = SQLAlchemy()


class Users(db.Model):
    __tablename__ = 'users'
    user_id = db.Column(db.Integer, primary_key=True)
    user_login = db.Column(db.String(32), unique=True, nullable=False)
    user_pwd_hash = db.Column(db.String(128), nullable=False)
    user_type = db.Column(db.Integer)

    def set_password(self, password):
        self.user_pwd_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.user_pwd_hash, password)

    def __init__(self, dict_incoming=False):
        if dict_incoming:
            self.user_login = dict_incoming['user_login']
            self.set_password(dict_incoming['user_pwd'])
            self.user_type = dict_incoming['user_type']


class Messages(db.Model):
    __tablename__ = 'messages'
    message_id = db.Column(db.Integer, primary_key=True, nullable=False)
    message_sender_user_id = db.Column(db.Integer, ForeignKey('users.user_id'), nullable=False)
    message_recipient_user_id = db.Column(db.Integer, ForeignKey('users.user_id'), nullable=False)
    message_text = db.Column(db.Text, nullable=False)
    message_datetime = db.Column(db.DateTime, nullable=False)
    message_status_code = db.Column(db.Integer, nullable=False)

    rel_sender_user_id = relationship("Users", foreign_keys=[message_sender_user_id])
    rel_recipient_user_id = relationship("Users", foreign_keys=[message_recipient_user_id])

    def __init__(self, dict_incoming=False):
        if dict_incoming:
            self.message_sender_user_id = dict_incoming['message_sender_user_id']
            self.message_recipient_user_id = dict_incoming['message_recipient_user_id']
            self.message_text = dict_incoming['message_text']
            #todo: check date zone
            self.message_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            self.message_status_code = dict_incoming['message_status_code']


class Errors(db.Model):
    __tablename__ = 'errors'
    error_id = db.Column(db.Integer, primary_key=True, nullable=False)
    error_text = db.Column(db.Text, nullable=False)
    error_datetime = db.Column(db.DateTime, nullable=False)

    def __init__(self, dict_incoming=False):
        if dict_incoming:
            self.error_text = dict_incoming['error_text']
            self.error_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")


