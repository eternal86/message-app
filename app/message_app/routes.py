#!flask/bin/python
from message_app import app
from flask import Flask, jsonify, abort, request, make_response, url_for, session
from flask.ext.httpauth import HTTPBasicAuth
from models import Users, Messages, Errors, db
from random import randint
import config
import sqlalchemy as sa

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(user_login, user_pwd):
    if 'user_type' in session:
        return True
    u = Users()
    el = u.query.filter_by(user_login=user_login).first()
    if el:
        if el.check_password(user_pwd):
            session['user_type'] = el.user_type
            session['user_login'] = el.user_login
            session['user_id'] = el.user_id
            return True
        else:
            return False
    else:
        return False


@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)


@app.errorhandler(400)
def not_found(error):
    return make_response(jsonify({'error': 'Bad request'}), 400)


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)


# of course it can be divided into different routes, but i had problems with js(didn't have much time),
# so had to do this BAD way
@app.route('/spa-api/api/v1.0/data', methods=['GET'])
@auth.login_required
def get_data():
    if session['user_type'] == config.USER_TYPE_USER:
        users_list = []
        el_list = Users.query.filter(Users.user_type == config.USER_TYPE_USER,
                                     Users.user_id != session['user_id']).all()
        if el_list:
            for el in el_list:
                users_list.append({'user_id': el.user_id, 'user_login': el.user_login})
        return jsonify({'users_list': users_list})

    errors_list = []
    if session['user_type'] == config.USER_TYPE_ADMIN:
        el_list = Errors.query.order_by(Errors.error_id.desc()).all()
        if el_list:
            for el in el_list:
                errors_list.append({'error_text': el.error_text, 'error_datetime': el.error_datetime})


        # spent 30 mins to do it with ORM, decided to write raw query. But still it is possible to do same with ORM
        sql = 'SELECT a.user_login, b.user_login, message_status_code, COUNT(message_id) AS total_messages FROM messages ' \
              'LEFT JOIN Users a ON message_sender_user_id = a.user_id LEFT JOIN Users b ' \
              'ON message_recipient_user_id = b.user_id GROUP BY message_status_code, a.user_login, b.user_login ' \
              'ORDER BY a.user_login, b.user_login, message_status_code ASC'
        result = db.engine.execute(sql)
        stats_dict = {config.STATUS_CODE_SUCCESS: {'count': 0, 'stats_list': []},
                      config.STATUS_CODE_NOTICE_1: {'count': 0, 'stats_list': []},
                      config.STATUS_CODE_NOTICE_2: {'count': 0, 'stats_list': []}}
        if result.rowcount > 0:
            for row in result:
                if row[2] == config.STATUS_CODE_SUCCESS:
                    status_text = 'STATUS_CODE_SUCCESS'
                elif row[2] == config.STATUS_CODE_NOTICE_1:
                    status_text = 'STATUS_CODE_NOTICE_1'
                elif row[2] == config.STATUS_CODE_NOTICE_2:
                    status_text = 'STATUS_CODE_NOTICE_2'

                stats_dict[row[2]]['count'] += row[3]
                stats_dict[row[2]]['status_text'] = status_text
                stats_dict[row[2]]['stats_list'].append(
                    {'sender_login': row[0], 'recipient_login': row[1], 'count': row[3]})
        return jsonify({'messages_stats': stats_dict, 'errors_list': errors_list})


@app.route('/spa-api/api/v1.0/data/<int:recipient_user_id>', methods=['GET'])
@auth.login_required
def get_messages(recipient_user_id):
    res = []
    # task = filter(lambda t: t['id'] == task_id, tasks)
    el_list = Messages.query.filter(sa.or_(sa.and_(Messages.message_sender_user_id == session['user_id'],
                                                   Messages.message_recipient_user_id == recipient_user_id),
                                           sa.and_(Messages.message_sender_user_id == recipient_user_id,
                                                   Messages.message_recipient_user_id == session['user_id']))).order_by(
        Messages.message_datetime.desc()).all()
    if el_list:
        for el in el_list:
            message_direction = '<-'
            if el.message_sender_user_id == session['user_id']:
                message_direction = '->'
            res.append({'message_datetime': el.message_datetime,
                        'message_recipient_login': el.rel_recipient_user_id.user_login,
                        'message_text': el.message_text, 'message_direction': message_direction})
    return jsonify({'messages': res})


@app.route('/spa-api/api/v1.0/data', methods=['POST'])
@auth.login_required
def create_message():
    if not request.json or not 'message_text' in request.json:
        abort(400)
    message_text = request.json['message_text']
    message_recipient_user_id = request.json['message_recipient_user_id']
    # 20% unsuccessful messages
    if randint(1, 2) == 1:
        # [1,2] = warning:still add into messages table, [3] = error: add only into errors table
        message_status_code = randint(config.STATUS_CODE_NOTICE_1, config.STATUS_CODE_ERROR)
    else:
        message_status_code = config.STATUS_CODE_SUCCESS
    dict_outg = {'message_sender_user_id': session['user_id'], 'message_recipient_user_id': message_recipient_user_id,
                 'message_text': message_text,
                 'message_status_code': message_status_code}
    record_message = Messages(dict_incoming=dict_outg)
    try:
        if message_status_code == config.STATUS_CODE_ERROR or True:
            # broad exception as example, of course i can specify
            raise Exception('Generated Error ~ 7%' + 'from all messages. Message: %s' % message_text)
        else:
            db.session.add(record_message)
            db.session.commit()
    # will not skip IntegrityError also if there will be no user as it is foreign key
    except Exception, e_message:
        db.session.remove()
        record_error = Errors(dict_incoming={'error_text': e_message[0]})
        db.session.add(record_error)
        db.session.commit()
    return jsonify(), 201


@app.route('/spa-api/api/v1.0/logout', methods=['GET'])
def logout():
    if 'user_type' in session:
        session.clear()
    return jsonify(), 201
