from flask.ext.sqlalchemy import SQLAlchemy
from message_app import app
from message_app import db
from message_app import models
from message_app import config
from time import sleep

with app.app_context():
    dict_outg = {'user_login': 'admin', 'user_pwd': 'admin', 'user_type': config.USER_TYPE_ADMIN}
    record_user = models.Users(dict_incoming=dict_outg)
    db.session.add(record_user)
    db.session.commit()

    users = ['user_1', 'user_2', 'user_3', 'user_4', 'user_5']
    for user in users:
        dict_outg = {'user_login': user, 'user_pwd': user, 'user_type': config.USER_TYPE_USER}
        record_user = models.Users(dict_incoming=dict_outg)
        db.session.add(record_user)
        db.session.commit()

    errors = ['dummy (psycopg2.IntegrityError) insert or update on table "messages" violates ...',
              'dummy (psycopg2.IntegrityError)  table "messages" violates ...',
              'dummy(psycopg2.IntegrityError) insert on table "messages" violates ...',
              'dummy (psycopg2.IntegrityError) update on table "messages" violates ...']
    for error in errors:
        dict_outg = {'error_text': error}
        record_error = models.Errors(dict_incoming=dict_outg)
        db.session.add(record_error)
        db.session.commit()
        sleep(1)
        print 'sleep...'
    print 'done'
